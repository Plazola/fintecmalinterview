package com.rafael.interview.Common

import com.rafael.interview.Interface.RetrofitService
import com.rafael.interview.Retrofit.RetrofitClient

object Common {
    private val BASE_URL = "https://fintecimal-test.herokuapp.com/api/"

    val retrofitService: RetrofitService
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitService::class.java)
}