package com.rafael.interview.entities

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rafael.interview.Interface.PlaceDao
import com.rafael.interview.Models.Places

@Database(
    entities = [Places::class],
    version = 1
)
abstract class PlacesDb : RoomDatabase (){
    abstract fun placeDao(): PlaceDao
    companion object {
        private var INSTANCE: PlacesDb? = null
        fun getInstance(context: Context):PlacesDb {
            if(INSTANCE == null){
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    PlacesDb::class.java,"places"
                ).build()
            }
            return INSTANCE!!
        }
    }
}