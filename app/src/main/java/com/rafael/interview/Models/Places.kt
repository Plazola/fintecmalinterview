package com.rafael.interview.Models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "places")
data class Places(
    @PrimaryKey()
    var streetName:String,
    var suburb: String,
    var visited: Boolean,
    var latitude: Double,
    var longitude: Double){
}