package com.rafael.interview.Models


data class PlacesApi(
    var streetName:String,
    var suburb: String,
    var visited: Boolean,
    var location: Location){
}