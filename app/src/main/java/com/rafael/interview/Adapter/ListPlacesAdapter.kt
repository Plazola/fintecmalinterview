package com.rafael.interview.Adapter

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.rafael.interview.Models.Places
import com.rafael.interview.R

class ListPlacesAdapter (private val context: Context, private val list: List<Places>, private val mListener: OnItemClickListener?) : BaseAdapter(){
    private lateinit var visited_text: TextView
    private lateinit var street_name_text: TextView
    private lateinit var suburb_text: TextView
    private lateinit var status_circle: ImageView
    private lateinit var container: CardView

    interface OnItemClickListener {
        fun onItemClick(places:Places)
    }

    override fun getCount(): Int {
        return list.size
    }
    override fun getItem(position: Int): Any {
        return position
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = convertView
        convertView = LayoutInflater.from(context).inflate(R.layout.places_item, parent, false)
        visited_text = convertView.findViewById(R.id.visited_text)
        street_name_text = convertView.findViewById(R.id.street_name_text)
        suburb_text = convertView.findViewById(R.id.suburb_text)
        status_circle = convertView.findViewById(R.id.status_circle)
        container = convertView.findViewById(R.id.container)
        container.setTag(list[position])

        street_name_text.text = list[position].streetName
        suburb_text.text = list[position].suburb
        if(list[position].visited){
            visited_text.setTextColor(Color.parseColor("#3ac2c2"))
            visited_text.text = "Visitada"
            status_circle.setImageResource(R.drawable.visited)
        } else {
            visited_text.setTextColor(Color.parseColor("#AAAAAA"))
            visited_text.text = "Pendiente"
            status_circle.setImageResource(R.drawable.pendig)
        }

        container.setOnClickListener(View.OnClickListener {
            var element = it.getTag() as Places
            mListener?.onItemClick(element)
        })
        return convertView
    }
}