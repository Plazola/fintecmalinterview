package com.rafael.interview.Contollers

import com.rafael.interview.MainActivity
import com.rafael.interview.Models.Location
import com.rafael.interview.Models.Places

class PlacesContoller (var model: Places,var view: MainActivity){
    fun getStreetName():String{
        return model.streetName
    }
    fun setStreetName(streetName:String){
        model.streetName = streetName
    }

    fun getSuburb():String{
        return model.suburb
    }
    fun setSuburb(suburb:String){
        model.suburb = suburb
    }

    fun getVisited():Boolean{
        return model.visited
    }
    fun setVisited(visited:Boolean){
        model.visited = visited
    }

   // fun getLocation():Location{
   //     return model.location
   // }
   // fun setLocation(location:Location){
   //     model.location = location
   // }
}