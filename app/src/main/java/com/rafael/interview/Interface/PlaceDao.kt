package com.rafael.interview.Interface

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.rafael.interview.Models.Places

@Dao
interface PlaceDao {
    @Query("SELECT * FROM Places")
    fun getAll():LiveData<List<Places>>

    @Query("SELECT * FROM Places WHERE streetName = :streetName")
    fun getByStreetName(streetName:String):Places

    @Query("SELECT * FROM Places WHERE streetName LIKE '%' || :streetName || '%'")
    fun getAllFilter(streetName:String): LiveData<List<Places>>

    @Update
    suspend fun update(places:Places)

    @Insert
    abstract suspend fun insert(places:Places)

}