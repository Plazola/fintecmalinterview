package com.rafael.interview.Interface

import retrofit2.Call
import retrofit2.http.GET
import com.rafael.interview.Models.PlacesApi

interface RetrofitService {
    @GET("interview")
    fun getPlacesList(): Call<List<PlacesApi>>
}