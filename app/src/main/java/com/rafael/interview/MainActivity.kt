package com.rafael.interview

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.rafael.interview.Adapter.ListPlacesAdapter
import com.rafael.interview.Common.Common
import dmax.dialog.SpotsDialog
import com.rafael.interview.Interface.PlaceDao
import com.rafael.interview.Interface.RetrofitService
import com.rafael.interview.Models.Places
import com.rafael.interview.Models.PlacesApi
import com.rafael.interview.entities.PlacesDb
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory


class MainActivity : AppCompatActivity(), OnMapReadyCallback,
    ListPlacesAdapter.OnItemClickListener {

    lateinit var mService: RetrofitService
    private lateinit var map: GoogleMap
    lateinit var listView: ListView
    var adapter: ListPlacesAdapter? = null
    lateinit var dialog: AlertDialog
    lateinit var placeDao: PlaceDao
    lateinit var viewMap: RelativeLayout
    lateinit var viewList: LinearLayout
    lateinit var accion_buttons: RelativeLayout

    lateinit var visited_text: TextView
    lateinit var street_name_text: TextView
    lateinit var suburb_text: TextView
    lateinit var status_circle: ImageView
    var map_activated = true
    lateinit var btn_navegar: Button
    lateinit var btn_visita: Button
    lateinit var placeSelect: Places
    lateinit var editTextSerch: EditText
    lateinit var list:List<Places>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dialog = SpotsDialog.Builder().setCancelable(false).setContext(this).build()
        mService = Common.retrofitService
        viewMap = findViewById(R.id.map_view)
        viewList = findViewById(R.id.list_view)

        visited_text = findViewById(R.id.visited_text)
        street_name_text = findViewById(R.id.street_name_text)
        suburb_text = findViewById(R.id.suburb_text)
        editTextSerch = findViewById(R.id.editTextSerch)
        accion_buttons = findViewById(R.id.accion_buttons)
        status_circle = findViewById(R.id.status_circle)

        btn_navegar = findViewById(R.id.btn_navegar)
        btn_visita = findViewById(R.id.btn_visita)

        listView = findViewById(R.id.listView)
        placeDao = PlacesDb.getInstance(this).placeDao()
        createList()
        createMapFragment()

        btn_navegar.setOnClickListener(View.OnClickListener {
            val gmmIntentUri =
                Uri.parse("geo:0,0?q=${placeSelect.latitude.toString()},${placeSelect.longitude.toString()}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        })

        btn_visita.setOnClickListener(View.OnClickListener {
            placeSelect.visited = true
            GlobalScope.launch {
                placeDao.update(placeSelect)
            }
            backToList()
            createList()
        })

        editTextSerch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                Log.e("test", s.toString())
                SearchList(s.toString())
            }
        })

    }

    private fun createList() {
        placeDao.getAll().observe(this, Observer {
            list = it as List<Places>
            if (list.size == 0) {
                getDataFromDb()
            } else {
                adapter = ListPlacesAdapter(this, list, this)
                listView.adapter = adapter
            }
        })
    }

    private fun SearchList(SearchKey: String) {
        if (!SearchKey.equals("")) {
            placeDao.getAllFilter(SearchKey).observe(this, Observer {
                list = it as List<Places>
                Log.e("lista",list.toString())
                adapter = ListPlacesAdapter(this, list, this)
                listView.adapter = adapter
            })
        }else {
            placeDao.getAll().observe(this, Observer {
                list = it as List<Places>
                adapter = ListPlacesAdapter(this, list, this)
                listView.adapter = adapter
            })
        }
    }

    private fun createMapFragment() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragmentMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    private fun createMarker(latitude: Double, longitude: Double, title: String, visited: Boolean) {
        map.clear()
        val pointInMap = LatLng(latitude, longitude)
        if (visited) {
            map.addMarker(
                MarkerOptions().position(pointInMap).title(title)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_visited_marker))
            )
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(pointInMap, 18f),
                4000,
                null
            )
        } else {
            map.addMarker(
                MarkerOptions().position(pointInMap).title(title)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
            )
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(pointInMap, 18f),
                4000,
                null
            )
        }

    }

    fun getDataFromDb() {
        dialog.show()
        mService.getPlacesList().enqueue(object : Callback<List<PlacesApi>> {
            override fun onFailure(call: Call<List<PlacesApi>>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<List<PlacesApi>>,
                response: Response<List<PlacesApi>>
            ) {
                response.body()?.forEach {
                    GlobalScope.launch {
                        placeDao.insert(
                            Places(
                                it.streetName,
                                it.suburb,
                                it.visited,
                                it.location.latitude,
                                it.location.longitude
                            )
                        )
                    }
                }
                dialog.dismiss()
                createList()
            }
        })
    }

    fun backToList() {
        viewList.visibility = View.VISIBLE
        viewMap.visibility = View.GONE
        map_activated == false;
    }

    override fun onItemClick(places: Places) {
        createMarker(places.latitude, places.longitude, places.streetName, places.visited)
        viewList.visibility = View.GONE
        viewMap.visibility = View.VISIBLE
        placeSelect = places
        if (places.visited) {
            visited_text.setTextColor(Color.parseColor("#3ac2c2"))
            visited_text.text = "Visitado"
            accion_buttons.visibility = View.GONE
            status_circle.setImageResource(R.drawable.visited)
        } else {
            visited_text.setTextColor(Color.parseColor("#AAAAAA"))
            visited_text.text = "Pendiente"
            accion_buttons.visibility = View.VISIBLE
            status_circle.setImageResource(R.drawable.pendig)
        }
        street_name_text.text = places.streetName
        suburb_text.text = places.suburb
        map_activated = true
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
    }

    override fun onBackPressed() {
        if (!map_activated) {
            super.onBackPressed()
            return
        } else {
            backToList()
        }
    }

}
